(use ezxdisp matchable miscmacros ssax)

(include "path-parser.scm")

(define world-map (with-input-from-file "World_map_-_low_resolution.svg"
	      (lambda () 
		(ssax:xml->sxml (current-input-port) '(( s . "http://www.w3.org/2000/svg"))))))


(define (relative-coords? c)
  (and (pair? c) (eq? (last c) 'relative)))

(define (normalise-coords c ref coords)
  (let ((normalised-coords (match c
                               ('horizontal-lineto (list (car coords) 0))
                               ('vertical-lineto (list 0 (car coords)))
                               (else coords)))
        (absolute? (member 'absolute coords)))
#;
(print "command " c " coords " coords " absolute? " absolute? " -> " normalised-coords " : " (if absolute? normalised-coords
                                                                                                 (map + normalised-coords (filter number?  ref))))
(if absolute? normalised-coords
    (map + (filter number? normalised-coords) ref))))

(define number-of-commands 0)
(define current-pos '(0 0))
(define origin #f)
(define (maybe-set-origin coords)
  (unless origin (set! origin coords)))

(define (execute-command window color width c)
  (inc! number-of-commands)
  (let* ((command (car c))
	 (coords (normalise-coords command current-pos (cdr c))))
  ;  (print "command " c)
    (set! current-pos (match command
                             ('line-to (ezx-line-2d window (car current-pos) (cadr current-pos) (car coords) (cadr coords) color width) coords)
                             ('horizontal-lineto (ezx-line-2d window (car current-pos) (cadr current-pos) (car coords) (cadr current-pos) color width) (list (car coords) (cadr current-pos)))
                             ('vertical-lineto (ezx-line-2d window (car current-pos) (cadr current-pos) (car current-pos) (cadr coords) color width) (list (car current-pos) (cadr coords)))
                             ('move-to (maybe-set-origin coords) coords)
                             ('close-path (ezx-line-2d window (car origin) (cadr origin) (car current-pos) (cadr current-pos) color width)
                                          (set! origin #f)
                                          '(0 0))
                             (else current-pos)))
   ))


(define test "M781.68,324.4l-2.31,8.68l-12.53,4.23l-3.75-4.4l-1.82,0.5l3.4,13.12l5.09,0.57l6.79,2.57v2.57l3.11-0.57l4.53-6.27v-5.13l2.55-5.13l2.83,0.57l-3.4-7.13l-0.52-4.59L781.68,324.4L781.68,324.4z")

(define window (ezx-init 950 620 "world"))
(define blue (make-ezx-color 0 0 1))

(define (draw path)
  (let loop ((p (parser path)))
    (cond ((null? p) 'done)
	  (else (execute-command window blue 2 (car p))
		(loop (cdr p))))))

;(draw test)

 (define paths (map (lambda (p) (cadar (cddadr p)) ) (cddr (fourth world-map))))

(time (for-each (lambda (p) (draw p)) paths))
 (ezx-redraw window)
(print "Processed " number-of-commands " commands.")
